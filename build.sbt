name := "testproject-with-gitlab-ci"

version := "0.1"

sbtVersion := "1.2.8"

scalaVersion := "2.12.8"

organization := "com.quandoo.data"
// https://mvnrepository.com/artifact/org.scalatest/scalatest
// https://mvnrepository.com/artifact/org.scala-lang/scala-library
libraryDependencies += "org.scala-lang" % "scala-library" % "2.12.8"
//libraryDependencies += "com.quandoo.data" %% "testproject" % "0.1"
publishTo := {
  val nexus = "http://nexus.quandoo.com/nexus/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/data-snapshots")
  else
    Some("releases"  at nexus + "content/repositories/data-releases/")
}
publishConfiguration := publishConfiguration.value.withOverwrite(true)
publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true)
//(realm, host, user, password) = sys.env.get("USERNAME")
//sys.env.get("PASSWORD")
credentials += Credentials(
  sys.env.getOrElse("NEXUS_REALM_CONFIG", ""),
  sys.env.getOrElse("NEXUS_HOST_CONFIG", ""),
  sys.env.getOrElse("NEXUS_USER_CONFIG", ""),
  sys.env.getOrElse("NEXUS_PASSWORD_CONFIG", ""))